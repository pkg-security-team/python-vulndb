Source: python-vulndb
Section: python
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Gianfranco Costamagna <locutusofborg@debian.org>
Build-Depends: debhelper-compat (= 12), dh-python, python3, python3-setuptools, python3-setuptools-git
Standards-Version: 4.6.2
Homepage: https://github.com/vulndb/python-sdk/
Vcs-Git: https://salsa.debian.org/pkg-security-team/python-vulndb.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/python-vulndb

Package: python3-vulndb
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Python3 tool to provide access to the vulndb information
 It's a Python3 SDK to access the vulnerability database.
 User, contributor and developer-friendly vulnerability database.
 The goal is to provide a vulnerability database which is:
  * Actionable, easy to read and understand for developers and sysadmins
    who need to fix the vulnerability
  * Easy to integrate by developers into any vulnerability scanner, report
    generator, penetration testing tool or related tool.
  * Trivial to contribute to, by using JSON to store the vulnerabilities
